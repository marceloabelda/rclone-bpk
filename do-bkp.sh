#!/bin/bash

SRC_DIR="/dir"
REMOTE_DEST="s3remote:mybucket/"
REMOTE_FOLDER="current/"
BKP_FOLDER="`date +%Y%m%d%H%M%S`"

rclone \
   --transfers=25 \
   --checkers=50 \
   -v \
   --checksum \
   sync \
   ${SRC_DIR} \
   ${REMOTE_DEST}${REMOTE_FOLDER} \
   --backup-dir \
   ${REMOTE_DEST}${BKP_FOLDER}
